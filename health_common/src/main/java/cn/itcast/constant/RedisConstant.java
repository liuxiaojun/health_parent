package cn.itcast.constant;

/**
 * @author : liuxiaojun
 * @version : V1.0
 * @date : 2019-07-19 12:57
 * @description :
 */
public class RedisConstant {
    //套餐图片所有图片名称
    public static final String SETMEAL_PIC_RESOURCES = "setmealPicResources";
    //套餐图片保存在数据库中的图片名称
    public static final String SETMEAL_PIC_DB_RESOURCES = "setmealPicDbResources";
}
