package com.itheima.service;

import cn.itcast.pojo.User;

/**
 * @author : liuxiaojun
 * @version : V1.0
 * @date : 2019-07-14 16:19
 * @description :
 */
public interface IUserService {
    User selectUserById(Integer id);
}
