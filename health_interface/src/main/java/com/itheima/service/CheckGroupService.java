package com.itheima.service;

import cn.itcast.entity.PageResult;
import cn.itcast.entity.QueryPageBean;
import cn.itcast.pojo.CheckGroup;

import java.util.List;
import java.util.Map;

/**
 * @author : liuxiaojun
 * @version : V1.0
 * @date : 2019-07-17 14:16
 * @description :
 */
public interface CheckGroupService {
    /**
     * 新增检查组
     *
     * @param checkGroup
     * @param checkItemids
     */
    void handleAdd(CheckGroup checkGroup, List<Integer> checkItemids);

    /**
     * 检查组分页查询
     *
     * @param queryPageBean
     * @return
     */
    PageResult findPage(QueryPageBean queryPageBean);

    /**
     * 根据查询组id查询更新信息
     *
     * @param id
     * @return
     */
    Map<String, Object> queryUpdateByCheckGroupId(Integer id);

    /**
     * 编辑检查组
     *
     * @param checkGroup
     * @param checkItemIds
     */
    void updateByCheckCroupAndCheckItemIds(CheckGroup checkGroup, List<Integer> checkItemIds);

    /**
     * 根据id删除检查组
     *
     * @param id
     */
    void deleteCheckGroupById(Integer id);
}
