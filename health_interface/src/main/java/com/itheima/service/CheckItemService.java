package com.itheima.service;

import cn.itcast.entity.PageResult;
import cn.itcast.entity.QueryPageBean;
import cn.itcast.pojo.CheckItem;

import java.util.List;

/**
 * @author : liuxiaojun
 * @version : V1.0
 * @date : 2019-07-16 14:26
 * @description :
 */
public interface CheckItemService {

    void add(CheckItem checkItem);

    PageResult findPage(QueryPageBean queryPageBean);

    void deleteCheckItemById(Integer id);

    List<CheckItem> queryAll();

    CheckItem queryCheckItemById(Integer id);

    void updateCheckItemById(CheckItem checkItem);

}
