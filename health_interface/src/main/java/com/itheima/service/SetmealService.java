package com.itheima.service;

import cn.itcast.entity.PageResult;
import cn.itcast.entity.QueryPageBean;
import cn.itcast.pojo.CheckGroup;
import cn.itcast.pojo.Setmeal;

import java.util.List;
import java.util.Map;

/**
 * @author : liuxiaojun
 * @version : V1.0
 * @date : 2019-07-18 15:11
 * @description :
 */
public interface SetmealService {
    PageResult findPage(QueryPageBean queryPageBean);

    List<CheckGroup> quaryAllCheckGroup();

    void add(Integer[] checkGroupIds, Setmeal setmeal);

    Map<String, Object> querySetMeal(Integer id);

    void updateSetmeal(Setmeal setmeal, List<Integer> checkGroupIds);

    void deleteSetMeal(Integer id);
}
