package com.itheima.service;

import cn.hutool.core.lang.UUID;
import cn.itcast.entity.PageResult;
import cn.itcast.entity.QueryPageBean;
import cn.itcast.pojo.CheckGroup;
import cn.itcast.pojo.Setmeal;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.SetmealDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : liuxiaojun
 * @version : V1.0
 * @date : 2019-07-18 15:12
 * @description :
 */
@Transactional
@Service(interfaceClass = SetmealService.class)
public class SetmealServiceImpl implements SetmealService{
    @Autowired
    SetmealDao setmealDao;
    /**
     * 分页查询
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        Page page = PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        List<Setmeal> Setmeals=setmealDao.findPage(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(),Setmeals);
    }

    /**
     * 查询所有检查组
     * @return
     */
    @Override
    public List<CheckGroup> quaryAllCheckGroup() {
        return setmealDao.quaryAllCheckGroup();
    }

    /**
     * 新增套餐
     * @param checkGroupIds
     * @param setmeal
     */
    @Override
    public void add(Integer[] checkGroupIds, Setmeal setmeal) {
        setmealDao.addSetmeal(setmeal);
        Integer setmealId = setmeal.getId();
        if (checkGroupIds.length>0){
            setmealDao.addCheckGroupIdsAndSetmealIds(checkGroupIds,setmealId);
        }
    }

    /**
     * 回显套餐内容
     * @param id
     * @return
     */
    @Override
    public Map<String, Object> querySetMeal(Integer id) {
        Setmeal setmeal = setmealDao.querySetMeal(id);
        List<Integer> checkGroupIds=setmealDao.queryCheckGroupIds(id);
        List<CheckGroup> checkGroups = setmealDao.quaryAllCheckGroup();
        Map<String, Object> data = new HashMap<>();
        data.put("setmeal",setmeal);
        data.put("checkGroupIds",checkGroupIds);
        data.put("checkGroups",checkGroups);
        return data;
    }

    /**
     * 套餐编辑
     * @param setmeal
     * @param checkGroupIds
     */
    @Override
    public void updateSetmeal(Setmeal setmeal, List<Integer> checkGroupIds) {
        setmealDao.deleteSetmealAndCheckgroup(setmeal.getId());
        setmealDao.updateSetmealById(setmeal);
        setmealDao.insertSetmealAndCheckgroup(setmeal.getId(),checkGroupIds);
    }

    /**
     * 删除套餐
     * @param id
     */
    @Override
    public void deleteSetMeal(Integer id) {
        setmealDao.deleteSetmealAndCheckgroup(id);
        setmealDao.deleteSetmealById(id);
    }
}
