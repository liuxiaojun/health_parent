package com.itheima.service;

import cn.itcast.pojo.User;
import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.IUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author : liuxiaojun
 * @version : V1.0
 * @date : 2019-07-14 16:21
 * @description :处理用户业务的Service
 */
@Service(interfaceClass = IUserService.class)
@Transactional
public class UserServiceImpl implements IUserService {

    @Autowired
    private IUserDao iUserDao;

    @Override
    public User selectUserById(Integer id) {
        return iUserDao.selectUserById(id);
    }
}
