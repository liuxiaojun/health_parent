package com.itheima.service;

import cn.itcast.entity.PageResult;
import cn.itcast.entity.QueryPageBean;
import cn.itcast.pojo.CheckItem;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.CheckItemDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author : liuxiaojun
 * @version : V1.0
 * @date : 2019-07-16 14:27
 * @description :负责检查项目的Service
 */
@Service(interfaceClass = CheckItemService.class)
@Transactional
public class CheckItemServiceImpl implements CheckItemService {

    @Autowired
    CheckItemDao checkItemDao;

    /**
     * 新增检查项
     *
     * @param checkItem
     */
    @Override
    public void add(CheckItem checkItem) {
        checkItemDao.add(checkItem);
    }

    /**
     * 分页查询
     *
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        //使用分页插件
        Page page = PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        System.out.println("page = " + page);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        List<CheckItem> checkItems = checkItemDao.findPage(queryPageBean.getQueryString());
        long total = page.getTotal();
        return new PageResult(total, checkItems);
    }

    /**
     * 根据id删除检查项
     *
     * @param id
     */
    @Override
    public void deleteCheckItemById(Integer id) {
        //查询检查项是否被引用
        Integer count = checkItemDao.findCountByCheckItemId(id);
        if (null !=count && count > 0){
            throw new RuntimeException("引用，不能删除");
        }else {
            //如果有引用提示用户
            checkItemDao.deleteCheckItemById(id);
        }
    }

    /**
     * 查询所有检查项
     * @return
     */
    @Override
    public List<CheckItem> queryAll() {
        return checkItemDao.queryAll();
    }

    /**
     * 回显检查项数据
     * @param id
     * @return
     */
    @Override
    public CheckItem queryCheckItemById(Integer id) {
        return checkItemDao.queryCheckItemById(id);
    }

    /**
     * 检查项编辑
     * @param
     */
    @Override
    public void updateCheckItemById(CheckItem checkItem) {

        if (checkItem.getId() != null){
            checkItemDao.updateCheckItemById(checkItem);
        }else {
            throw new RuntimeException("服务异常,无法拿到数据");
        }

    }
}
