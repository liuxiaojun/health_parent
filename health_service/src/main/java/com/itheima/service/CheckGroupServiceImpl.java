package com.itheima.service;

import cn.hutool.core.collection.CollectionUtil;
import cn.itcast.entity.PageResult;
import cn.itcast.entity.QueryPageBean;
import cn.itcast.pojo.CheckGroup;
import cn.itcast.pojo.CheckItem;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.CheckGroupDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : liuxiaojun
 * @version : V1.0
 * @date : 2019-07-17 14:17
 * @description :负责检查组的Service
 */
@Service(interfaceClass = CheckGroupService.class)
@Transactional
public class CheckGroupServiceImpl implements CheckGroupService {
    @Autowired
    CheckGroupDao checkGroupDao;

    /**
     * 新增检查组
     *
     * @param checkGroup
     * @param checkItemids
     */
    @Override
    public void handleAdd(CheckGroup checkGroup, List<Integer> checkItemids) {
        //保存检查组的基本信息并返回主健
        checkGroupDao.add(checkGroup);
        Integer checkGroupId = checkGroup.getId();
        //循环保存检查组和检查项的关系t_checkgroup_checkitem
        if (CollectionUtil.isNotEmpty(checkItemids)) {
            for (Integer checkItemid : checkItemids) {
                checkGroupDao.setCheckGroupAndCheckItem(checkGroupId, checkItemid);
            }
        }
    }

    /**
     * 检查组分页查询
     *
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        Page page = PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        List<CheckGroup> checkGroupList = checkGroupDao.findPage(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(), checkGroupList);
    }

    /**
     * @param id
     * @return
     */
    @Override
    public Map<String, Object> queryUpdateByCheckGroupId(Integer id) {
        //查询检查组的基本信息
        CheckGroup checkGroup = checkGroupDao.queryCheckGroupById(id);
        //查询所有的检查项目
        List<CheckItem> tableData = checkGroupDao.queryAllCheckItems();
        //根据检查组id查询检查项id
        List<Integer> checkitemIds = checkGroupDao.queryCheckitemIdsByCheckGroupId(id);
        Map<String, Object> data = new HashMap<>();
        data.put("checkGroup", checkGroup);
        data.put("tableData", tableData);
        data.put("checkitemIds", checkitemIds);
        return data;
    }

    /**
     * 编辑检查组
     *
     * @param checkGroup
     * @param checkItemIds
     */
    @Override
    public void updateByCheckCroupAndCheckItemIds(CheckGroup checkGroup, List<Integer> checkItemIds) {
        //更新检查组
        checkGroupDao.updateByCheckGroupId(checkGroup);
        //根据checkGroupId删除原有关系表
        checkGroupDao.deleteByCheckGroupId(checkGroup.getId());
        //更新表关系
        checkGroupDao.updateByCheckGroupIdAndCheckItemId(checkGroup.getId(), checkItemIds);
    }

    /**
     * 根据id删除检查组
     * @param id
     */
    @Override
    public void deleteCheckGroupById(Integer id) {
        checkGroupDao.updateCheckGroupById(id);
        checkGroupDao.deleteCheckGroupAndCheckItemById(id);
    }
}
