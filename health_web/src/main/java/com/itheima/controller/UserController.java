package com.itheima.controller;

import cn.itcast.pojo.User;
import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.service.IUserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : liuxiaojun
 * @version : V1.0
 * @date : 2019-07-14 17:19
 * @description :
 */
@RestController
@RequestMapping("user")
public class UserController {
    @Reference
    IUserService iUserService;

    @RequestMapping("selectUserById")
    public User selectUserById(@RequestParam("id")Integer id) {
        return iUserService.selectUserById(id);
    }
}
