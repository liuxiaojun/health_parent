package com.itheima.controller;

import cn.itcast.constant.MessageConstant;
import cn.itcast.entity.PageResult;
import cn.itcast.entity.QueryPageBean;
import cn.itcast.entity.Result;
import cn.itcast.pojo.CheckGroup;
import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.service.CheckGroupService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author : liuxiaojun
 * @version : V1.0
 * @date : 2019-07-14 17:19
 * @description :负责检查项目的Controller
 */
@RestController
@RequestMapping("check_group")
public class CheckGroupController {
    @Reference
    CheckGroupService checkGroupService;

    /**
     * 新增检查组
     *
     * @param checkGroup
     * @param checkItemids
     * @return
     */
    @RequestMapping("handleAdd")
    public Result add(@RequestParam("checkitemIds") List<Integer> checkItemids, @RequestBody CheckGroup checkGroup) {
        try {
            checkGroupService.handleAdd(checkGroup, checkItemids);
            return new Result(true, MessageConstant.ADD_CHECKGROUP_SUCCESS, null);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_CHECKGROUP_FAIL, null);
        }
    }

    /**
     * 检查组分页查询
     * @param queryPageBean
     * @return
     */
    @RequestMapping("findPage")
    public Result findPage(@RequestBody QueryPageBean queryPageBean){
        try {
            PageResult pageResult = checkGroupService.findPage(queryPageBean);
            return new Result(true,"",pageResult);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"网络异常，请联系管理员",null);
        }
    }

    /**
     * 根据查询组id查询更新信息
     * @param id
     * @return
     */
    @RequestMapping("queryUpdate")
    public Result queryUpdateByCheckGroupId(Integer id){
        try {
            Map<String,Object> data=checkGroupService.queryUpdateByCheckGroupId(id);
            return new Result(true,"",data);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"服务器异常，请联系管理员",null);
        }
    }

    /**
     * 编辑检查组
     * @param checkGroup
     * @param checkItemIds
     * @return
     */
    @RequestMapping("handleEdit")
    public Result updateByCheckCroupAndCheckItemIds(@RequestBody CheckGroup checkGroup,@RequestParam("checkitemIds")List<Integer> checkItemIds){
        try {
            checkGroupService.updateByCheckCroupAndCheckItemIds(checkGroup,checkItemIds);
            return new Result(true,MessageConstant.EDIT_CHECKGROUP_SUCCESS,null);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(true,MessageConstant.EDIT_CHECKGROUP_FAIL,null);
        }
    }

    /**
     * 根据id删除检查组
     * @param id
     * @return
     */
    @RequestMapping("deleteCheckGroupById")
    public Result deleteCheckGroupById(Integer id){
        try {
            checkGroupService.deleteCheckGroupById(id);
            return new Result(true,MessageConstant.DELETE_CHECKGROUP_SUCCESS,null);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.DELETE_CHECKGROUP_FAIL,null);
        }
    }
}
