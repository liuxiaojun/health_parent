package com.itheima.controller;

import cn.itcast.constant.MessageConstant;
import cn.itcast.entity.PageResult;
import cn.itcast.entity.QueryPageBean;
import cn.itcast.entity.Result;
import cn.itcast.pojo.CheckItem;
import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.service.CheckItemService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author : liuxiaojun
 * @version : V1.0
 * @date : 2019-07-14 17:19
 * @description :负责检查项目的Controller
 */
@RestController
@RequestMapping("check_item")
public class CheckItemController {
    @Reference
    CheckItemService checkItemService;

    /**
     * 新增检查项
     * 当页面传递的参数类型是Map/Pojo 参数列表中需要加上@RequestBody注解否则无法进行映射
     * 当页面传递的参数类型是基本类型且名称于参数列表中参数名称一致时候，不需要加注解
     * 除以上两种情况外，其他情况下参数列表中都需要加上@RequestParam注解
     *
     * @param checkItem
     * @return
     */
    @RequestMapping("add")
    public Result add(@RequestBody CheckItem checkItem) {

        try {
            checkItemService.add(checkItem);
            return new Result(true, MessageConstant.ADD_CHECKITEM_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_CHECKITEM_FAIL);
        }

    }

    /**
     * 分页查询
     *
     * @param
     * @return
     */
    @RequestMapping("findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        return checkItemService.findPage(queryPageBean);
    }

    /**
     * 根据id删除检查项
     *
     * @param id
     * @return
     */
    @RequestMapping("deleteCheckItemById")
    public Result deleteCheckItemById(@RequestParam("id") Integer id) {
        try {
            checkItemService.deleteCheckItemById(id);
            return new Result(true, MessageConstant.DELETE_CHECKITEM_SUCCESS);
        } catch (RuntimeException e) {
            e.printStackTrace();
            return new Result(false, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.DELETE_CHECKITEM_FAIL);
        }
    }

    /**
     * 查询所有检查项目
     * @return
     */
    @RequestMapping("queryAll")
    public Result queryAll() {
        try {
            List<CheckItem> checkItems = checkItemService.queryAll();
            return new Result(true,"",checkItems);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.QUERY_CHECKITEM_FAIL ,null);
        }
    }

    /**
     * 回显检查项数据
     * @param id
     * @return
     */
    @RequestMapping("queryCheckItemById")
    public Result queryCheckItemById(Integer id){
        try {
            CheckItem checkItem = checkItemService.queryCheckItemById(id);
            return new Result(true,"",checkItem);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(true,"网络异常，请联系管理员");
        }
    }

    /**
     * 检查项编辑
     * @param
     * @return
     */
    @RequestMapping("handleEdit")
    public Result updateCheckItemById(@RequestBody CheckItem checkItem){
        try {
            checkItemService.updateCheckItemById(checkItem);
            return new Result(true,MessageConstant.EDIT_CHECKITEM_SUCCESS,null);
        }catch (RuntimeException e){
            return new Result(false,e.getMessage(),null);
        }
        catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.EDIT_CHECKITEM_FAIL,null);
        }
    }
}
