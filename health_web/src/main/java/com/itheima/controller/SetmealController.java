package com.itheima.controller;

import cn.hutool.core.lang.UUID;
import cn.itcast.constant.MessageConstant;
import cn.itcast.constant.RedisConstant;
import cn.itcast.entity.PageResult;
import cn.itcast.entity.QueryPageBean;
import cn.itcast.entity.Result;
import cn.itcast.pojo.CheckGroup;
import cn.itcast.pojo.Setmeal;
import cn.itcast.utils.QiniuUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import redis.clients.jedis.JedisPool;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author : liuxiaojun
 * @version : V1.0
 * @date : 2019-07-18 14:56
 * @description :
 */
@RequestMapping("set_meal")
@RestController
public class SetmealController {

    @Reference
    SetmealService setmealService;

    @Autowired
    JedisPool jedisPool;

    /**
     * 分页查询
     *
     * @param queryPageBean
     * @return
     */
    @RequestMapping("findPage")
    public Result findPage(@RequestBody QueryPageBean queryPageBean) {
        try {
            PageResult data = setmealService.findPage(queryPageBean);
            return new Result(true, "", data);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "网络异常");
        }
    }

    /**
     * 查询所有检查组
     *
     * @return
     */
    @RequestMapping("queryAllCheckGroup")
    public Result quaryAllCheckGroup() {
        try {
            List<CheckGroup> tableData = setmealService.quaryAllCheckGroup();
            return new Result(true, "", tableData);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "网络异常");
        }
    }

    /**
     * 文件上传
     * @param imgFile
     * @return
     */
    @RequestMapping("upload")
    public Result upload(MultipartFile imgFile) {
        //MultipartFile是spring类型，代表HTML中form data方式上传的文件，包含二进制数据+文件名称
        //修改文件名称(保证不重复)
        //substring：文件后缀名
        String substring = imgFile.getOriginalFilename().
                substring(imgFile.getOriginalFilename().lastIndexOf("."));
        //获取随机字符串
        String str = UUID.randomUUID().toString();
        //上传的文件名称
        String fileName = str+substring;

        //将保存到七牛云上的文件名称存入redis
        jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_RESOURCES,fileName);


        //调用七牛SDK上传
        try {
            QiniuUtils.upload2Qiniu(imgFile.getBytes(), fileName);
            return new Result(true,MessageConstant.PIC_UPLOAD_SUCCESS,fileName);
        } catch (IOException e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.PIC_UPLOAD_FAIL);
        }
    }

    /**
     * 新增套餐
     * @param checkGroupIds
     * @param setmeal
     * @return
     */
    @RequestMapping("add")
    public Result add(Integer[] checkGroupIds, @RequestBody Setmeal setmeal){
        try {
            setmealService.add(checkGroupIds,setmeal);
            //将保存到数据库上的文件名称存入redis
            jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_DB_RESOURCES,setmeal.getImg());

            return new Result(true,MessageConstant.ADD_SETMEAL_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.ADD_SETMEAL_FAIL);
        }
    }

    /**
     * 回显套餐内容
     * @param id
     * @return
     */
    @RequestMapping("querySetMeal")
    public Result querySetMeal(@RequestParam Integer id){
        try {
            Map<String,Object> data=setmealService.querySetMeal(id);
            return new Result(true,"",data);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"网络异常");
        }
    }

    /**
     * 套餐编辑
     * @param setmeal
     * @param checkGroupIds
     * @return
     */
    @RequestMapping("edit")
    public Result updateSetmeal(@RequestBody Setmeal setmeal,@RequestParam List<Integer> checkGroupIds){

        try {
            setmealService.updateSetmeal(setmeal,checkGroupIds);
            return new Result(true,MessageConstant.EDIT_SETMEAL_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.EDIT_SETMEAL_FAIL);
        }
    }

    /**
     * 删除套餐
     * @param id
     * @return
     */
    @RequestMapping("deleteSetMeal")
    public Result deleteSetMeal(@RequestParam Integer id){
        try {
            setmealService.deleteSetMeal(id);
            return new Result(true,MessageConstant.DELETE_SETMEAL_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.DELETE_SETMEAL_FAIL);
        }
    }


}
