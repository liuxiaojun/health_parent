package com.itheima.dao;

import cn.itcast.pojo.User;
import org.apache.ibatis.annotations.Param;

/**
 * @author : liuxiaojun
 * @version : V1.0
 * @date : 2019-07-15 16:19
 * @description :
 */
public interface IUserDao {
    User selectUserById(@Param("id") Integer id);
}
