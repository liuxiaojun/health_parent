package com.itheima.dao;

import cn.itcast.pojo.CheckGroup;
import cn.itcast.pojo.CheckItem;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author : liuxiaojun
 * @version : V1.0
 * @date : 2019-07-17 14:20
 * @description :负责检查项组的Dao
 */
public interface CheckGroupDao {
    /**
     * 新增检查组
     *
     * @param checkGroup
     */
    void add(CheckGroup checkGroup);

    /**
     * 新增CheckGroup和CheckItem表关系
     *
     * @param checkGroupId
     * @param checkItemid
     */
    void setCheckGroupAndCheckItem(@Param("checkGroupId") Integer checkGroupId, @Param("checkItemid") Integer checkItemid);

    /**
     * 检查组分页查询
     *
     * @param queryString
     * @return
     */
    List<CheckGroup> findPage(@Param("queryString") String queryString);

    /**
     * 查询检查组的基本信息
     *
     * @param id
     * @return
     */
    CheckGroup queryCheckGroupById(@Param("id") Integer id);

    /**
     * 查询所有的检查项目
     *
     * @return
     */
    List<CheckItem> queryAllCheckItems();

    /**
     * 根据检查组id查询检查项id
     *
     * @param id
     * @return
     */
    List<Integer> queryCheckitemIdsByCheckGroupId(@Param("id") Integer id);

    /**
     * 更新检查组
     * @param checkGroup
     */
    void updateByCheckGroupId(CheckGroup checkGroup);

    /**
     * 根据checkGroupId删除原有关系表
     * @param id
     */
    void deleteByCheckGroupId(Integer id);

    /**
     * 更新表关系
     * @param id
     * @param checkItemIds
     */
    void updateByCheckGroupIdAndCheckItemId(@Param("id") Integer id, @Param("checkItemIds") List<Integer> checkItemIds);

    /**
     * 根据id删除检查组(逻辑删除)
     * @param id
     */
    void updateCheckGroupById(@Param("id") Integer id);

    /**
     * 根据id删除多表关系
     * @param id
     */
    void deleteCheckGroupAndCheckItemById(@Param("id") Integer id);
}
