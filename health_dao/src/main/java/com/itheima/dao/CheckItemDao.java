package com.itheima.dao;

import cn.itcast.pojo.CheckItem;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author : liuxiaojun
 * @version : V1.0
 * @date : 2019-07-16 14:40
 * @description :负责检查项目的Dao
 */
public interface CheckItemDao {
    /**
     * 新增检查项
     * @param checkItem
     */
    void add(CheckItem checkItem);

    /**
     * 分页查询
     * @param queryString
     * @return
     */
    List<CheckItem> findPage(@Param("queryString") String queryString);

    /**
     * 根据id删除检查项
     * @param id
     */
    void deleteCheckItemById(@Param("id") Integer id);

    /**
     * 查询检查项是否被引用
     * @param id
     * @return
     */
    Integer findCountByCheckItemId(@Param("id") Integer id);

    /**
     * 查询所有检查项
     * @return
     */
    List<CheckItem> queryAll();

    /**
     * 回显检查项数据
     * @param id
     * @return
     */
    CheckItem queryCheckItemById(@Param("id") Integer id);

    /**
     * 检查项编辑
     * @param
     */
    void updateCheckItemById(CheckItem checkItem);
}
