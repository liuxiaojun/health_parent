package com.itheima.dao;

import cn.itcast.pojo.CheckGroup;
import cn.itcast.pojo.Setmeal;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author : liuxiaojun
 * @version : V1.0
 * @date : 2019-07-18 15:13
 * @description :
 */
public interface SetmealDao {
    /**
     * 分页查询
     * @param queryString
     * @return
     */
    List<Setmeal> findPage(@Param("queryString") String queryString);

    /**
     * 查询所有检查组
     * @return
     */
    List<CheckGroup> quaryAllCheckGroup();

    /**
     * 新增套餐和检查组关系
     * @param
     * @param
     */
    void addCheckGroupIdsAndSetmealIds(@Param("checkGroupIds") Integer[] checkGroupIds, @Param("setmealId") Integer setmealId);

    /**
     * 新增检查套餐
     * @param setmeal
     */
    void addSetmeal(Setmeal setmeal);

    /**
     * 回显套餐内容
     * @param id
     * @return
     */
    Setmeal querySetMeal(@Param("id") Integer id);

    /**
     * 根据套餐id查询检查组id
     * @param id
     * @return
     */
    List<Integer> queryCheckGroupIds(@Param("id") Integer id);

    /**
     * 删除表关系
     * @param id
     */
    void deleteSetmealAndCheckgroup(@Param("id") Integer id);

    /**
     * 更新套餐表
     * @param setmeal
     */
    void updateSetmealById(Setmeal setmeal);

    /**
     * 更新表关系
     * @param setmealId
     * @param checkGroupIds
     */
    void insertSetmealAndCheckgroup(@Param("setmealId") Integer setmealId, @Param("checkGroupIds") List<Integer> checkGroupIds);

    /**
     * 逻辑删除套餐
     * @param id
     */
    void deleteSetmealById(@Param("id") Integer id);
}
