import cn.itcast.pojo.User;
import com.itheima.dao.IUserDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author : liuxiaojun
 * @version : V1.0
 * @date : 2019-07-15 16:27
 * @description :
 */
@ContextConfiguration(locations = "classpath*:applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class Test01 {
    @Autowired
    private IUserDao iUserDao;
    @Test
    public void selectUserById(){
        User user = iUserDao.selectUserById(1);
        System.out.println("user = " + user);
    }
}
