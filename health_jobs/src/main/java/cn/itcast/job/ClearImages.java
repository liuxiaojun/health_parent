package cn.itcast.job;


import cn.hutool.core.collection.CollectionUtil;
import cn.itcast.constant.RedisConstant;
import cn.itcast.utils.QiniuUtils;
import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.JedisPool;
import java.util.Set;

/**
 * @author : liuxiaojun
 * @version : V1.0
 * @date : 2019-07-19 14:49
 * @description :
 */
public class ClearImages {

    @Autowired
    JedisPool jedisPool;

    public void doClear() {
        System.out.println("jedisPool = " + jedisPool);
        Set<String> sdiff = jedisPool.getResource().sdiff(RedisConstant.SETMEAL_PIC_RESOURCES, RedisConstant.SETMEAL_PIC_DB_RESOURCES);
        System.out.println(sdiff.toString());
        if (CollectionUtil.isNotEmpty(sdiff)) {
            for (String filename : sdiff) {
                //七牛云删除无效图片
                QiniuUtils.deleteFileFromQiniu(filename);
                //redis删除无效图片
                jedisPool.getResource().srem(RedisConstant.SETMEAL_PIC_RESOURCES,filename);
            }
        }
    }
}
